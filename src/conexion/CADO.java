package conexion;

import java.sql.*;

public class CADO {

    Connection cn;

    public boolean Conectar() {
        String strHost;
        String strPort;
        String strDatabase;
        String strUser;
        String strPassword;
        String strCadenaConexion;
        try {
            strHost = "192.168.11.154:3306";
            strDatabase = "acadsis";
            strUser = "acadsis";
            strPassword = "acadsis-celad0n065";
            strCadenaConexion = "jdbc:mysql://" + strHost + "/" + strDatabase;
            System.out.println(strCadenaConexion);
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection(strCadenaConexion, strUser, strPassword);
            //System.out.println("OK ... CONECTO CON DRIVER");
            return true;

        } catch (Exception ex) {
            System.out.println(ex.getMessage().toString());
            return false;
            //      System.out.println(ex.getMessage());
        }
    }

    public Connection Connect() {
        String strHost;
        String strPort;
        String strDatabase;
        String strUser;
        String strPassword;
        String strCadenaConexion;
        try {
            strHost = "192.168.11.154";
            strDatabase = "pelicula";
            strUser = "root";
            strPassword = "root";
            strCadenaConexion = "jdbc:mysql://" + strHost + "/" + strDatabase;
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection(strCadenaConexion, strUser, strPassword);
            System.out.println("OK ... CONECTO CON DRIVER");
            return cn;

        } catch (Exception ex) {
            System.out.println(ex.getMessage().toString());
            return null;
            //      System.out.println(ex.getMessage());
        }
    }

    public boolean Ejecutar(String sql) {
        Statement st;
        try {
            this.Conectar();
            st = cn.createStatement();
            st.execute(sql);
            System.out.println("OK");
            return true;
        } catch (Exception ex) {
            System.out.println("Error aa...." + ex.toString());
            return false;
        }
    }

    public boolean consultasimple(String con) {
        String valor = con;
        Statement st;
        try {
            if (this.Conectar() == true) {
                st = cn.createStatement();
                st.executeUpdate(valor);
            }
            return true;
        } catch (Exception ex) {
            System.out.println("Error...." + ex.toString());
            return false;
        }
    }

    public ResultSet consultaconresp(String op) {
        String valor = op;
        Statement st;
        ResultSet resul = null;
        try {
            if (this.Conectar() == true) {
                st = cn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                resul = st.executeQuery(valor);

            }
            return resul;
        } catch (Exception ex) {
            System.out.println("Error...." + ex.toString());
            return null;
        }
    }

}
