package vista;

import javax.swing.table.AbstractTableModel;

public class Modelo extends AbstractTableModel {

    private String[] columnNames = null;
    private String[][] values = null;

    public Modelo(String[] Columnas, String[][] Datos) {
        columnNames = Columnas;
        values = Datos;
    }

    public int getRowCount() {
        return values.length;
    }

    public void RemoveElement() {
        String[][] nuevo = new String[1][columnNames.length];
        values = nuevo;
        this.fireTableDataChanged();
    }

    public int getColumnCount() {
        return values[0].length;
    }

    public Object getValueAt(int row, int column) {
        return values[row][column];
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}
