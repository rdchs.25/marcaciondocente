/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.sql.ResultSet;
import Formularios.RegistrarHuella;
import conexion.CADO;
import Formularios.MarcacionHuella1;

/**
 *
 * @author David
 */
public class ListarDocentes extends javax.swing.JFrame {

    /**
     * Creates new form ListarDocentes
     */
    public ListarDocentes() {
        initComponents();
        listardocentes();
    }
    CADO cd = new CADO();
    int totaldocentes;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDocentes = new javax.swing.JTable();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtTotal = new javax.swing.JTextPane();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Lista de Docentes");

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel1.setText("                                                      LISTA DE DOCENTES");

        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ModificarUsuario.png"))); // NOI18N
        btnModificar.setText("MODIFICAR");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Cerrar.png"))); // NOI18N
        btnCerrar.setText("CERRAR");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        tblDocentes.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{}
        ));
        jScrollPane1.setViewportView(tblDocentes);

        txtBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarActionPerformed(evt);
            }
        });
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        txtTotal.setEditable(false);
        jScrollPane2.setViewportView(txtTotal);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/visto.png"))); // NOI18N
        jButton1.setText("ASISTENCIA");
        jButton1.setMaximumSize(new java.awt.Dimension(127, 41));
        jButton1.setMinimumSize(new java.awt.Dimension(127, 41));
        jButton1.setPreferredSize(new java.awt.Dimension(127, 41));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addGap(0, 0, Short.MAX_VALUE)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(18, 18, 18)
                                                                        .addComponent(btnBuscar)
                                                                        .addGap(18, 18, 18)
                                                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                        .addComponent(btnModificar)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                        .addComponent(btnCerrar)
                                                                        .addGap(172, 172, 172)))))
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 702, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(btnBuscar))
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnModificar)
                                        .addComponent(btnCerrar)
                                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>    

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        this.dispose();
    }

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        if (tblDocentes.getSelectedRow() != -1) {
            int iddocente = Integer.parseInt(tblDocentes.getModel().getValueAt(tblDocentes.getSelectedRow(), 0).toString());
            RegistrarHuella frm = new RegistrarHuella(iddocente);
            frm.setVisible(true);
            this.dispose();

        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un docente", "Mensaje del Sistema", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void txtBuscarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {
        // TODO add your handling code here:
        listardocentes();
    }

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        listardocentes();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        /*if (tblDocentes.getSelectedRow() != -1) {
            int idpersona = Integer.parseInt(tblDocentes.getModel().getValueAt(tblDocentes.getSelectedRow(), 0).toString());
            int idsedelogin = 1;
            MarcacionHuella1 frm = new MarcacionHuella1(idpersona, idsedelogin);
            frm.setVisible(true);
            this.dispose();

        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un docente", "Mensaje del Sistema", JOptionPane.WARNING_MESSAGE);
        }*/
    }

    public void listardocentes() {

        totaldocentes = 0;
        String buscar = this.txtBuscar.getText();

        String sql = "";
        if (buscar != null && !buscar.equals("")) {
            sql = "SELECT Docentes_docente.user_ptr_id, Docentes_docente.ApellidoPaterno AS ApellidoPaterno, "
                    + "Docentes_docente.ApellidoMaterno AS ApellidoMaterno,Docentes_docente.Nombres AS Nombres, "
                    + "Docentes_docente.Huella "
                    + "FROM Docentes_docente "
                    + "where Docentes_docente.ApellidoPaterno like '%" + buscar + "%' "
                    + "or Docentes_docente.ApellidoMaterno like '%" + buscar + "%' "
                    + "or Docentes_docente.Nombres like '%" + buscar + "%' "
                    + "order by Docentes_docente.ApellidoPaterno asc";
        } else {
            sql = "SELECT Docentes_docente.user_ptr_id, Docentes_docente.ApellidoPaterno AS ApellidoPaterno, "
                    + "Docentes_docente.ApellidoMaterno AS ApellidoMaterno,Docentes_docente.Nombres AS Nombres, "
                    + "Docentes_docente.Huella "
                    + "FROM Docentes_docente "
                    + "order by Docentes_docente.ApellidoPaterno asc";
        }
        ResultSet rs = cd.consultaconresp(sql);
        DefaultTableModel dt = new DefaultTableModel();
        dt.addColumn("ID");
        dt.addColumn("Apellido Paterno");
        dt.addColumn("Apellido Materno");
        dt.addColumn("Nombres");
        dt.addColumn("Huella");

        try {

            while (rs.next()) {
                Vector vFila = new Vector();
                vFila.addElement(rs.getInt(1));
                vFila.addElement(rs.getString(2));
                vFila.addElement(rs.getString(3));
                vFila.addElement(rs.getString(4));

                if (rs.getString(5) != null) {
                    vFila.addElement("SI");
                    totaldocentes++;
                } else {
                    vFila.addElement("NO");
                }

                dt.addRow(vFila);

            }
//

            this.tblDocentes.setModel(dt);
            this.txtTotal.setText("Tienen Huella " + totaldocentes + " docentes");
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }

    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblDocentes;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextPane txtTotal;
    // End of variables declaration      

}
