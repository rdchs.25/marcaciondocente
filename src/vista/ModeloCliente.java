package vista;

import bean.cliente;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class ModeloCliente extends AbstractTableModel {

    private String[] columnas = {"ID", "DNI", "APELLIDOS", "NOMBRES"};
    private List<cliente> datos = new ArrayList<cliente>();

    public ModeloCliente(List<cliente> datos) {
        this.datos = datos;
    }

    public int getRowCount() {
        return datos.size();
    }

    public int getColumnCount() {
        return columnas.length;
    }

    public String getColumnName(int columnIndex) {
        return columnas[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Object valor = null;

        if (columnIndex == 0) {
            valor = datos.get(rowIndex).getId();
        } else if (columnIndex == 1) {
            valor = datos.get(rowIndex).getDni();
        } else if (columnIndex == 1) {
            valor = datos.get(rowIndex).getApellidos();
        } else if (columnIndex == 1) {
            valor = datos.get(rowIndex).getNombres();
        }

        return valor;
    }
}
