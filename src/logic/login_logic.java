/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import bean.usuario_bean;
import dao.login_dao;

/**
 *
 * @author Babastre
 */
public class login_logic {

    public boolean ingresar(usuario_bean obj) {

        boolean ya = false;
        try {
            login_dao dl = new login_dao();
            ya = dl.ingresar(obj);
            return ya;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    public usuario_bean consultarusuario(String login, String clave) {
        try {
            login_dao dl = new login_dao();

            return dl.consultarusuario(login, clave);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
