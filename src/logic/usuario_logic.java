/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import bean.usuario_bean;
import dao.usuario_dao;
import java.util.List;

/**
 *
 * @author ELIAS
 */
public class usuario_logic {

    usuario_dao dao = new usuario_dao();

    public boolean insertar(usuario_bean cliente) {

        return dao.Insertar(cliente);

    }

    public String[][] consultaruser() {
        return dao.consultaruser();
    }

    public boolean eliminaruser(String id) {
        return dao.Eliminar(id);
    }

}
