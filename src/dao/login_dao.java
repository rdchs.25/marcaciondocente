/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.usuario_bean;
import conexion.CADO;
import java.sql.ResultSet;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class login_dao {

    String usuario;
    String clave;

    public boolean ingresar(usuario_bean obj) throws IOException {
        usuario = obj.getLogin();
        clave = obj.getClave();
        String data = "{\n\"usuario\": \"" + usuario + "\",\n\"password\": \"" + clave + "\"\n}";
        System.out.println(data);
        String url = "http://190.116.176.42:8085/loginauth/";
        boolean login = post(url, data);
        return login;
    }
    public usuario_bean consultarusuario(String login, String clave) {

        try {
            encriptacion obj2 = new encriptacion();
            CADO cd = new CADO();
            ResultSet res;
            String oper = "SELECT * FROM usuario where login='" + login + "' and password='" + obj2.encriptar(clave) + "';";
            res = cd.consultaconresp(oper);
            usuario_bean ub = new usuario_bean();
            res.next();
            ub.setClave(res.getObject("password").toString());
            ub.setEstado(res.getObject("estado").toString());
            ub.setId_usuario(res.getObject("id").toString());
            ub.setLogin(res.getObject("login").toString());
            ub.setIdsede(Integer.parseInt(res.getObject("idsede").toString()));
            return ub;

        } catch (Exception ex) {
            System.out.println("Error...." + ex.toString());
            return null;
        }
    }
    public boolean post(String url, String data) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, data);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("authorization", "token df42e7f09eebf99eb2a0e033b27ff1a92221a6d1")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "2ef73446-103e-4414-c78b-7cd25b5bcb82")
                .build();

        Response response = client.newCall(request).execute();
        Boolean issuccess = verificarRespuesta(response.body().string());

        return issuccess;
    }
    public Boolean verificarRespuesta(String cadenajson) {
        Boolean respuesta = false;
        try {
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(cadenajson);
            // verificamos que el resultado sea success = True
            Boolean resultado = (Boolean) json.get("success");
            System.out.println(resultado);
            if (resultado) {
                respuesta = true;
            }
            return respuesta;
        } catch (ParseException e) {
            return respuesta;
        }

    }
}
