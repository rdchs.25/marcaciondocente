/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.usuario_bean;
import conexion.CADO;
import java.sql.ResultSet;

/**
 *
 * @author ELIAS
 */
public class usuario_dao {

    CADO ocado = new CADO();
    CADO cd = new CADO();

    public boolean Insertar(usuario_bean usuario) {
        try {
            String sql = "INSERT INTO usuario(nombre,login,clave,estado,tipo) VALUES('" + usuario.getNombre() + "','" + usuario.getLogin() + "','" + usuario.getClave() + "','Activo','Operador');";
            return ocado.consultasimple(sql);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean Eliminar(String id) {
        try {
            String sql = "UPDATE usuario SET estado='Inactivo' WHERE id_usuario=" + id;
            return ocado.Ejecutar(sql);
        } catch (Exception ex) {
            return false;
        }

    }

    public String[][] consultaruser() {
        int i = 0;

        try {

            ResultSet res;
            String oper = "SELECT * FROM usuario WHERE estado='N';";
            res = cd.consultaconresp(oper);
            //res.next();
            int n = 0;

            while (res.next()) {
                n++;
            }

            String[][] matriz = new String[n][6];

            res.beforeFirst();
            while (res.next()) {
                //System.out.println("pos1 "+res.getObject(1).toString());
                matriz[i][0] = res.getObject(1).toString();
                matriz[i][1] = res.getObject(2).toString();
                matriz[i][2] = res.getObject(3).toString();
                matriz[i][3] = res.getObject(4).toString();
                matriz[i][4] = res.getObject(5).toString();
                matriz[i][5] = res.getObject(6).toString();
                i++;
            }

            return matriz;

        } catch (Exception ex) {
            System.out.println("Error...." + ex.toString());
            return null;
        }
    }

}
