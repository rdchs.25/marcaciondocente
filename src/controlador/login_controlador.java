/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import bean.usuario_bean;
import logic.login_logic;

/**
 *
 * @author Babastre
 */
public class login_controlador {

    login_logic ll = new login_logic();

    public boolean ingresar(usuario_bean obj) {

        return ll.ingresar(obj);
    }

    public usuario_bean consultarusuario(String login, String clave) {
        return ll.consultarusuario(login, clave);
    }

}
